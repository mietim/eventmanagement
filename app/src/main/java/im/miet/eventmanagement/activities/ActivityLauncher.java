/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement.activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import im.miet.eventmanagement.authentication.EventmanagementAuthenticator;

/**
 * Created by vlad on 27.09.15.
 */
public class ActivityLauncher extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkAccount();
    }

    private void checkAccount() {
        final Intent mainIntent = new Intent(this,MainActivity.class);
        AccountManager am = AccountManager.get(getApplicationContext());
        if(am.getAccountsByType(EventmanagementAuthenticator.ACCOUNT_TYPE).length == 0) {
            am.addAccount(EventmanagementAuthenticator.ACCOUNT_TYPE, EventmanagementAuthenticator.AUTHTOKEN_TYPE, null, null, this, new AccountManagerCallback<Bundle>() {
                @Override
                public void run(AccountManagerFuture<Bundle> future) {
                    try {
                        future.getResult();
                        startActivity(mainIntent);
                    } catch (Exception e) {
                        e.printStackTrace();
                        finish();
                    }
                }
            }, null);
        } else {
            startActivity(mainIntent);
            finish();
        }
    }
}
