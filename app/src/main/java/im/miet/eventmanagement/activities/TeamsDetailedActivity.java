/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement.activities;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import im.miet.eventmanagement.HttpHelper;
import im.miet.eventmanagement.R;
import im.miet.eventmanagement.dataclasses.Team;
import im.miet.eventmanagement.fragments.UsersListFragment;
import im.miet.eventmanagement.fragments.TeamsDescriptionFragment;

/**
 * Created by vlad on 18.08.15.
 */
public class TeamsDetailedActivity extends FragmentActivity implements Callback {

    public static final String URL_TAG = "im.miet.eventmanagement.url";
    private String url;
    private Team team;
    private Call call;
    Toolbar toolbar;
    private boolean isRefreshing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(null);
        setContentView(R.layout.teams_detailed);
        if (savedInstanceState == null) {
            url = getIntent().getStringExtra(URL_TAG);
        } else {
            url = savedInstanceState.getString(URL_TAG);
        }
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.main_menu);
        toolbar.setOnMenuItemClickListener(new MenuListener());
        fetchData(url);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(URL_TAG, url);
    }

    private void fetchData(String url) {
        call = HttpHelper.getInstance().callGetRequest(url, this);
    }

    @Override
    public void onFailure(Request request, IOException e) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(TeamsDetailedActivity.this, getResources().getString(R.string.err_bad_connection),
                        Toast.LENGTH_LONG).show();
            }
        });
        stopRefreshing();
    }

    private void constructNewLayout() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TeamsDescriptionFragment descriptionFragment = new TeamsDescriptionFragment();
                descriptionFragment.setData(team);
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.
                        id.descriptionContainer, descriptionFragment, TeamsDescriptionFragment.class.getName());
                if (team.members != null) {
                    UsersListFragment listFragment = UsersListFragment.newCustomDataInstance(new ArrayList<>(Arrays.asList(team.members)));
                    transaction.replace(R.id.usersListContainer, listFragment, UsersListFragment.class.getName());
                }
                transaction.commit();
                Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
                toolbar.setTitle(team.name);
            }
        });
    }

    private void refreshAll() {
        isRefreshing = true;
        View progressbar = findViewById(R.id.progressBar);
        progressbar.setVisibility(View.VISIBLE);
        toolbar.getMenu().getItem(0).setVisible(false);
        fetchData(url);
    }

    private void stopRefreshing() {
        isRefreshing = false;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                View progressbar = findViewById(R.id.progressBar);
                progressbar.setVisibility(View.GONE);
                toolbar.getMenu().getItem(0).setVisible(true);
            }
        });

    }

    @Override
    public void onResponse(Response response) throws IOException {
        if (call.isCanceled()) {
            return;
        }
        String dataString = response.body().string();
        response.body().close();
        try {
            ObjectMapper mapper = new ObjectMapper();
            team = mapper.readValue(dataString, Team.class);
            constructNewLayout();
        } catch (Exception e) {
            Log.d("JSON ", e.getMessage());
        } finally {
            if(isRefreshing) {
                stopRefreshing();
            }
        }

    }

    class MenuListener implements android.support.v7.widget.Toolbar.OnMenuItemClickListener {
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            int id = item.getItemId();
            if (id == R.id.action_refresh) {
                refreshAll();
                return true;
            }
            return false;
        }
    }

}
