/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement.activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;


import java.util.ArrayList;

import im.miet.eventmanagement.R;
import im.miet.eventmanagement.authentication.EventmanagementAuthenticator;
import im.miet.eventmanagement.fragments.NavigationFragment;
import im.miet.eventmanagement.fragments.RequestFragment;
import im.miet.eventmanagement.fragments.TeamsGridFragment;
import im.miet.eventmanagement.fragments.UsersDetailedFragment;
import im.miet.eventmanagement.fragments.UsersListFragment;

/**
 * Created by vlad on 07.08.15.
 */
public class MainActivity extends FragmentActivity {
    private Toolbar toolbar;
    private boolean isRefreshing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity_container);
        toolbar = (android.support.v7.widget.Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        toolbar.inflateMenu(R.menu.main_menu);
        toolbar.setOnMenuItemClickListener(new MenuListener());
        if(savedInstanceState == null) {
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.add(R.id.navigationContainer, new NavigationFragment(),
                    NavigationFragment.FRAGMENT_TAG);
            transaction.commit();
        }
    }

    public ArrayList<RequestFragment> getAllRequestFragments() {
        ArrayList<RequestFragment> fragments = new ArrayList<>();

        RequestFragment usersList = (RequestFragment)getFragmentManager()
                .findFragmentByTag(UsersListFragment.class.getName());
        RequestFragment usersDetailed = (RequestFragment)getFragmentManager()
                .findFragmentByTag(UsersDetailedFragment.class.getName());
        RequestFragment teams = (RequestFragment)getFragmentManager()
                .findFragmentByTag(TeamsGridFragment.class.getName());

        if (usersList != null) fragments.add(usersList);
        if (usersDetailed != null) fragments.add(usersDetailed);
        if (teams != null) fragments.add(teams);
        return fragments;
    }

    private void refreshAll() {
        isRefreshing = true;
        View progressbar = findViewById(R.id.progressBar);
        progressbar.setVisibility(View.VISIBLE);
        toolbar.getMenu().getItem(0).setVisible(false);
        ArrayList<RequestFragment> fragments = getAllRequestFragments();
        for (RequestFragment fragment : fragments) {
            fragment.refresh();
        }
    }

    private void exit() {
        AccountManager am = AccountManager.get(getApplicationContext());
        Account account = am.getAccountsByType(EventmanagementAuthenticator.ACCOUNT_TYPE)[0];
        am.removeAccount(account, null, null);
        startActivity(new Intent(this, ActivityLauncher.class));
        finish();
    }

    public void onRefreshingStopped() {
        if(!isRefreshing) return;
        ArrayList<RequestFragment> fragments = getAllRequestFragments();
        for (RequestFragment fragment : fragments) {
            if(fragment.isPending()) {
                return;
            }
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                View progressbar = findViewById(R.id.progressBar);
                progressbar.setVisibility(View.GONE);
                toolbar.getMenu().getItem(0).setVisible(true);
            }
        });
    }

    class MenuListener implements android.support.v7.widget.Toolbar.OnMenuItemClickListener {
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            int id = item.getItemId();
            if (id == R.id.action_refresh) {
                refreshAll();
                return true;
            }
            if (id == R.id.action_exit) {
                exit();
                return true;
            }
            return false;
        }
    }

}


