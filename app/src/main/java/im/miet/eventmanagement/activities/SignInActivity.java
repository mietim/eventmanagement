/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement.activities;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

import im.miet.eventmanagement.HttpHelper;
import im.miet.eventmanagement.R;
import im.miet.eventmanagement.authentication.EventmanagementAuthenticator;

/**
 * Created by vlad on 22.09.15.
 */
public class SignInActivity extends AccountAuthenticatorActivity implements Callback {

    private Call call;
    private String username;
    private String password;
    private String basicCredentials;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        ((Toolbar)findViewById(R.id.toolbar)).setTitle(getString(R.string.sign_in_title));
        findViewById(R.id.login_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }

    @Override
    public void onFailure(Request request, IOException e) {
        if(call.isCanceled()) {
            return;
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(SignInActivity.this, getResources().getString(R.string.err_bad_connection),
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onResponse(final Response response) throws IOException {
        if(call.isCanceled()) {
            return;
        }
        int code = response.code();
        if (code == 200) {
            confirmCredentials();
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d("AUTH",response.request().header("Authorization"));
                    Toast.makeText(SignInActivity.this,"Wrong cred",Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void hideKeyboard() {
        LinearLayout loginLayout = (LinearLayout) findViewById(R.id.signin_root);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(loginLayout.getWindowToken(), 0);
    }

    private void login() {
        hideKeyboard();
        username = ((EditText)findViewById(R.id.username)).getText().toString();
        password = ((EditText)findViewById(R.id.password)).getText().toString();
        basicCredentials = com.squareup.okhttp.Credentials.basic(username, password);
        call = HttpHelper.getInstance().checkCredentials(this,basicCredentials);
    }

    private void confirmCredentials() {
        Bundle result = new Bundle();
        AccountManager am =  AccountManager.get(getApplicationContext());
        Account account = new Account(username, EventmanagementAuthenticator.ACCOUNT_TYPE);
        if (am.addAccountExplicitly(account, password, new Bundle())) {
            result.putString(AccountManager.KEY_ACCOUNT_NAME, account.name);
            result.putString(AccountManager.KEY_ACCOUNT_TYPE, account.type);
            result.putString(AccountManager.KEY_AUTHTOKEN, basicCredentials);
            am.setAuthToken(account, EventmanagementAuthenticator.AUTHTOKEN_TYPE, basicCredentials);
        } else {
            result.putString(AccountManager.KEY_ERROR_MESSAGE, "User already exists");
        }
        setAccountAuthenticatorResult(result);
        setResult(RESULT_OK);
        finish();
    }
}
