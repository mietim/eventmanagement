/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement.activities;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import im.miet.eventmanagement.R;
import im.miet.eventmanagement.ServerUrl;
import im.miet.eventmanagement.fragments.TeamsGridFragment;

/**
 * Created by vlad on 23.08.15.
 */
public class TeamSelectorActivity extends Activity {

    public static final String RESULT_TAG = "im.miet.eventmanagement.TeamSelectorActivity.result";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.teams_selector_layout);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.team_select_title));
        Bundle args = new Bundle();
        args.putInt(TeamsGridFragment.REACTION_MODE,TeamsGridFragment.MODE_SELECT);
        args.putString(TeamsGridFragment.URL_TAG, ServerUrl.TEAMS_URL);
        TeamsGridFragment teamsGridFragment = new TeamsGridFragment();
        teamsGridFragment.setArguments(args);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.selectorGridContainer,teamsGridFragment);
        transaction.commit();
    }

    public void returnResult(Intent intent) {
        setResult(RESULT_OK,intent);
        finish();
    }
}
