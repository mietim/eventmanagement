/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement.dataclasses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by vlad on 09.08.15.
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class User {
    public int id;
    public String username;
    public String firstName;
    public String lastName;
    public String middleName;
    public String url;
    public String telephone;
    public String email;
    public TeamMembership teamMembership;
    public Status status;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return !(url != null ? !url.equals(user.url) : user.url != null);

    }

    @Override
    public int hashCode() {
        return url != null ? url.hashCode() : 0;
    }
}
