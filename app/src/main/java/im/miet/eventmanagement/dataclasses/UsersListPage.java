/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement.dataclasses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by vlad on 21.08.15.
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class UsersListPage {
    public int count;
    public String next;
    public String previous;
    @JsonProperty("results")
    public User[] users;
}
