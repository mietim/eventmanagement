/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement.dataclasses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by vlad on 12.08.15.
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class Team {
    public int id;
    public String name;
    public String url;
    public String description;
    public User curator;
    public User[] members;

    public static Team withoutTeamInstance() {
        Team team = new Team();
        team.name = "Нет команды";
        team.description = "";
        team.url = null;
        return team;
    }
}
