/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement.dataclasses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by vlad on 26.08.15.
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class TeamMembership {
    public String teamName;
    public String team;
    public String role;
    public String url;
}
