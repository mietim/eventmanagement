/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement.dataclasses;

import android.content.Context;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import im.miet.eventmanagement.R;

/**
 * Created by vlad on 14.08.15.
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class Status {
    public static final int PRESENCE_STATUS_ABSENT = 0;
    public static final int PRESENCE_STATUS_PRESENT = 1;
    public static final int PRESENCE_STATUS_LEFT = 2;
    public int status;
    public String text;
    public String url;

    public Status() {
    }

    public Status(Context context, int id) {
        status = id;
        text = context.getResources().getStringArray(R.array.Statuses)[id];
        //url = //get url
    }
}
