/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

import im.miet.eventmanagement.fragments.EndlessScrollingFragment;

/**
 * Created by vlad on 23.08.15.
 */
public class EndlessGridScrollListener extends RecyclerView.OnScrollListener  {
    private static final int VISIBLE_THRESHOLD = 5;
    private StaggeredGridLayoutManager layoutManager;
    private EndlessScrollingFragment fragment;

    public EndlessGridScrollListener(StaggeredGridLayoutManager layoutManager, EndlessScrollingFragment fragment) {
        this.layoutManager = layoutManager;
        this.fragment = fragment;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView,dx,dy);
        int visibleItemCount = recyclerView.getChildCount();
        int totalItemCount = layoutManager.getItemCount();
        int firstVisibleItems[] = new int[3];
        layoutManager.findFirstVisibleItemPositions(firstVisibleItems);
        if (fragment.canBeScrolled() && (totalItemCount - visibleItemCount)
                <= (firstVisibleItems[2] + VISIBLE_THRESHOLD)) {
            fragment.fetchNextPage();
        }
    }
}
