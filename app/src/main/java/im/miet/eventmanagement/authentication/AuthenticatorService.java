/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement.authentication;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by vlad on 27.09.15.
 */
public class AuthenticatorService extends Service {

    private EventmanagementAuthenticator auth;

    @Override
    public void onCreate() {
        super.onCreate();
        auth = new EventmanagementAuthenticator(getApplicationContext());
    }

    @Override
    public IBinder onBind(Intent intent) {
        return auth.getIBinder();
    }

}
