/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement.factories;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import im.miet.eventmanagement.R;

/**
 * Created by vlad on 12.08.15.
 */
public class DetailedHeaderFactory extends ViewFactory {

    private String title;

    public DetailedHeaderFactory(Context context, ViewGroup parent, String title) {
        super(context,parent);
        this.title = title;
        mContext = context;
    }

    @Override
    public View generate() {
        View view = LayoutInflater.from(mContext).inflate(R.layout.detailed_header, mParent, false);
        ((TextView)view.findViewById(R.id.title)).setText(title);
        return view;
    }
}
