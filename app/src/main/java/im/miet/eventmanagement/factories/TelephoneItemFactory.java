/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement.factories;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import im.miet.eventmanagement.R;

/**
 * Created by vlad on 16.08.15.
 */
public class TelephoneItemFactory extends SimpleItemFactory {

    public TelephoneItemFactory(Context context, ViewGroup parent, String telephone, String secondaryText) {
        super(context, parent, telephone, secondaryText);
    }

    @Override
    public View generate() {
        View view = LayoutInflater.from(mContext).inflate(R.layout.detailed_telephone_item, mParent, false );
        ((TextView)view.findViewById(R.id.primary)).setText(primary);
        ((TextView)view.findViewById(R.id.secondary)).setText(secondary);
        if(!secondary.equals("")) {
            view.findViewById(R.id.call_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Uri number = Uri.parse("tel:" + primary.trim());
                    Intent dial = new Intent(Intent.ACTION_CALL, number);
                    mContext.startActivity(dial);
                }
            });
        }
        return view;
    }
}
