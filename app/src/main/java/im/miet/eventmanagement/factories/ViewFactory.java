/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement.factories;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by vlad on 12.08.15.
 */
public abstract class ViewFactory {
    protected Context mContext;
    protected ViewGroup mParent;
    public ViewFactory(Context context, ViewGroup parent) {
        mContext = context;
        mParent = parent;
    }
    public abstract View generate();
}



