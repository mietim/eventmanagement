/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement.factories;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import im.miet.eventmanagement.R;

/**
 * Created by vlad on 12.08.15.
 */
public class SimpleItemFactory extends ViewFactory {
    protected String primary;
    protected String secondary;

    public SimpleItemFactory(Context context, ViewGroup parent, String primaryText, String secondaryText) {
        super(context,parent);
        primary = primaryText;
        secondary = secondaryText;
    }

    @Override
    public View generate() {
        View view = LayoutInflater.from(mContext).inflate(R.layout.detailed_simple_item, mParent, false );
        ((TextView)view.findViewById(R.id.primary)).setText(primary);
        ((TextView)view.findViewById(R.id.secondary)).setText(secondary);
        return view;
    }
}
