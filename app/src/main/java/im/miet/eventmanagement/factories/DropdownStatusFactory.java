/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement.factories;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;

import im.miet.eventmanagement.R;
import im.miet.eventmanagement.adapters.StatusSpinnerAdapter;
import im.miet.eventmanagement.dataclasses.User;

/**
 * Created by vlad on 14.08.15.
 */
public class DropdownStatusFactory extends ViewFactory {

    private String secondary;
    private StatusSpinnerAdapter adapter;
    private User attendee;


    public DropdownStatusFactory(Context context, ViewGroup parent,
                               String secondary, StatusSpinnerAdapter adapter, User attendee) {
        super(context, parent);
        this.secondary = secondary;
        this.adapter = adapter;
        this.attendee = attendee;
    }

    @Override
    public View generate() {
        View view = LayoutInflater.from(mContext).inflate(R.layout.detailed_simple_dropdown, mParent, false);
        Spinner spinner = ((Spinner) view.findViewById(R.id.spinner));
        spinner.setAdapter(adapter);
        spinner.setSelection(attendee.status.status);
        spinner.setOnItemSelectedListener(new StatusSpinnerAdapter.OnItemSelectedListener(mContext, attendee));
        ((TextView)view.findViewById(R.id.secondary)).setText(secondary);
        return view;
    }
}
