/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.content.Context;
import android.net.Credentials;
import android.os.Bundle;
import android.util.Log;

import com.squareup.okhttp.Cache;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.File;
import java.io.IOException;

import im.miet.eventmanagement.authentication.EventmanagementAuthenticator;

/**
 * Created by vlad on 21.08.15.
 */
public class HttpHelper {

    private OkHttpClient okHttpClient;

    private static class HelperHolder {
        private final static HttpHelper helperInstance = new HttpHelper();
    }

    private HttpHelper() {
        okHttpClient = new OkHttpClient();
        setCache();
    }

    private void setCache() {
        int cacheSize = 10 * 1024 * 1024; // 10 MiB
        Cache cache = new Cache(Application.getAppContext().getCacheDir(), cacheSize);
        okHttpClient.setCache(cache);
    }

    public static HttpHelper getInstance() {
        return HelperHolder.helperInstance;
    }

    public OkHttpClient getOkHttpClient() {
        return okHttpClient;
    }

    private String getAccountToken() {
        AccountManager am = AccountManager.get(Application.getAppContext());
        Account account = am.getAccountsByType(EventmanagementAuthenticator.ACCOUNT_TYPE)[0];
        return am.peekAuthToken(account, EventmanagementAuthenticator.AUTHTOKEN_TYPE);
    }

    public Call callGetRequest(String url, Callback callback) {
        Request request = new Request.Builder()
                .header("Authorization", getAccountToken())
                .url(url)
                .build();
        Call call = okHttpClient.newCall(request);
        call.enqueue(callback);
        return call;
    }

    public void callPutRequest(String url, String body) {
        Log.d(getClass().getName(),url);
        Request request = new Request.Builder()
                .header("Authorization", getAccountToken())
                .url(url)
                .put(RequestBody.create(MediaType.parse("application/json"), body))
                .build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
            }
        });
    }

    public Call checkCredentials(Callback callback, String credentialsBasic) {
        Request request = new Request.Builder()
                .head()
                .header("Authorization", credentialsBasic)
                .url(ServerUrl.EVENT_URI)
                .build();
        Call call = okHttpClient.newCall(request);
        call.enqueue(callback);
        return call;
    }
}
