/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement;

/**
 * Created by vlad on 22.08.15.
 */
public class ServerUrl {
    private static final String DOMAIN = "http://events.miet.im";
    //private static final String DOMAIN = "http://miet.im";
    public static final String API_ENTRY = DOMAIN + "/api/";
    public static final String EVENT_URI = DOMAIN + "/api/event/";
    public static final String ATTENDEES_URL = DOMAIN + "/api/users/attendees/";
    public static final String TEAMS_URL = DOMAIN + "/api/groups/";
    public static final String ORGANIZERS_URL = DOMAIN + "/api/users/organizers/";
}
