/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import im.miet.eventmanagement.HttpHelper;
import im.miet.eventmanagement.R;
import im.miet.eventmanagement.dataclasses.User;
import im.miet.eventmanagement.dataclasses.Status;
import im.miet.eventmanagement.fragments.UsersListFragment;

/**
 * Created by vlad on 14.08.15.
 */
public class StatusSpinnerAdapter extends BaseAdapter {
    private Context mContext;
    private String[] dataSet;

    public StatusSpinnerAdapter(Context context) {
        mContext = context;
        dataSet = mContext.getResources().getStringArray(R.array.Statuses);
    }

    @Override
    public int getCount() {
        return dataSet.length;
    }

    @Override
    public Object getItem(int position) {
        return dataSet[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
        }
        ((TextView)view.findViewById(android.R.id.text1)).setText((String)getItem(position));
        return view;
    }

    public static class OnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        private boolean firstLaunch = true;
        Context mContext;
        User user;

        public OnItemSelectedListener(Context context, User user) {
            mContext = context;
            this.user = user;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if(!firstLaunch) {
                HttpHelper.getInstance().callPutRequest(user.status.url,Integer.toString(position));
                UsersListFragment listFragment = (UsersListFragment)((Activity)mContext)
                        .getFragmentManager()
                        .findFragmentByTag(UsersListFragment.class.getName());
                if (listFragment != null) {
                    listFragment.changeStatus(user.id, position);
                }
            } else {
                firstLaunch = false;
            }
        }
        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    }
}
