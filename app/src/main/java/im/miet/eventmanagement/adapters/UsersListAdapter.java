/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement.adapters;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;

import java.util.ArrayList;

import im.miet.eventmanagement.HttpHelper;
import im.miet.eventmanagement.R;
import im.miet.eventmanagement.dataclasses.Status;
import im.miet.eventmanagement.dataclasses.User;
import im.miet.eventmanagement.fragments.UsersDetailedFragment;

/**
 * Created by vlad on 07.08.15.
 */
public class UsersListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<User> dataSet;
    private Context mContext;

    public UsersListAdapter(Context context) {
        mContext = context;
        dataSet = new ArrayList<>();
    }

    public ArrayList<User> getData() {
        return dataSet;
    }

    public void setData(ArrayList<User> newDataSet) {
        dataSet = newDataSet;
        ((Activity) mContext).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
    }

    public User findById(final int id) {
        return CollectionUtils.find(dataSet, new Predicate<User>() {
            @Override
            public boolean evaluate(User object) {
                return (object.id == id);
            }
        });
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final User user = dataSet.get(position);
        ((ItemHolder) holder).fullName.setText(user.lastName + " " + user.firstName);
        ((ItemHolder) holder).team.setText(user.teamMembership.teamName);
        setStatus((ItemHolder) holder, user.status);

        ((ItemHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UsersDetailedFragment detailedFragment = new UsersDetailedFragment();
                Bundle args = new Bundle();
                args.putString(UsersDetailedFragment.URL_TAG, user.url);
                detailedFragment.setArguments(args);
                FragmentTransaction transaction = ((Activity) mContext).getFragmentManager()
                        .beginTransaction();
                transaction.replace(R.id.usersDetailedContainer, detailedFragment,
                        UsersDetailedFragment.class.getName());
                transaction.commit();
            }
        });

        ((ItemHolder) holder).statusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user.status.status == Status.PRESENCE_STATUS_PRESENT) {
                    user.status.status = Status.PRESENCE_STATUS_ABSENT;
                } else {
                    user.status.status = Status.PRESENCE_STATUS_PRESENT;
                }
                HttpHelper.getInstance().callPutRequest(user.status.url, Integer.toString(user.status.status));
                notifyItemChanged(position);
                notifyDetailedFragment(((ItemHolder) holder).mContext, user.id, user.status);
            }
        });

        ((ItemHolder) holder).itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (user.telephone.equals("")) {
                    Toast.makeText(((ItemHolder) holder).mContext, R.string.err_no_telephone, Toast.LENGTH_LONG).show();
                    return false;
                }
                Uri number = Uri.parse("tel:" + user.telephone.trim());
                Intent dial = new Intent(Intent.ACTION_CALL, number);
                ((ItemHolder) holder).mContext.startActivity(dial);
                return true;
            }
        });
    }

    private void notifyDetailedFragment(Context context, int userId, Status status) {
        UsersDetailedFragment detailedFragment = (UsersDetailedFragment) ((Activity) context)
                .getFragmentManager()
                .findFragmentByTag(UsersDetailedFragment.class.getName());
        if (detailedFragment != null) {
            detailedFragment.changeStatus(userId, status);
        }
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item, parent, false);
        return new ItemHolder(view, parent.getContext());
    }

    private void setStatus(ItemHolder holder, Status status) {
        switch (status.status) {
            case Status.PRESENCE_STATUS_PRESENT:
                holder.statusButton.setBackgroundResource(R.drawable.icon_status_present);
                break;
            default:
                holder.statusButton.setBackgroundResource(R.drawable.icon_status_absent);
                break;
        }
    }

    public static class ItemHolder extends RecyclerView.ViewHolder {

        public TextView fullName;
        public TextView team;
        public Button statusButton;
        public View itemView;
        public Context mContext;

        public ItemHolder(View itemView, Context context) {
            super(itemView);
            this.itemView = itemView;
            mContext = context;
            fullName = (TextView) itemView.findViewById(R.id.fullName);
            team = (TextView) itemView.findViewById(R.id.team);
            statusButton = (Button) itemView.findViewById(R.id.status);
        }
        /*
        public void setOnClick(final String url) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UsersDetailedFragment detailedFragment = new UsersDetailedFragment();
                    Bundle args = new Bundle();
                    args.putString(UsersDetailedFragment.URL_TAG, url);
                    detailedFragment.setArguments(args);
                    FragmentTransaction transaction = ((Activity) mContext).getFragmentManager()
                            .beginTransaction();
                    transaction.replace(R.id.usersDetailedContainer, detailedFragment,
                            UsersDetailedFragment.FRAGMENT_TAG);
                    transaction.commit();
                }
            });
        }
        */
    }

    public static class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildLayoutPosition(view);
            if (position == 0) {
                outRect.top = space;
            }
            if (position == parent.getAdapter().getItemCount()) {
                outRect.bottom = space;
            }
        }
    }
}
