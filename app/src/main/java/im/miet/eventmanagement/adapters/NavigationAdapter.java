/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import im.miet.eventmanagement.R;
import im.miet.eventmanagement.dataclasses.Team;

/**
 * Created by vlad on 16.08.15.
 */
public class NavigationAdapter extends ArrayAdapter<String> {
    private int selectedItem = 0;

    public NavigationAdapter(Context context, int resource) {
        super(context, resource);
    }

    public NavigationAdapter(Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);
    }

    public NavigationAdapter(Context context, int resource, String[] objects) {
        super(context, resource, objects);
    }

    public NavigationAdapter(Context context, int resource, int textViewResourceId, String[] objects) {
        super(context, resource, textViewResourceId, objects);
    }

    public NavigationAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
    }

    public NavigationAdapter(Context context, int resource, int textViewResourceId, List<String> objects) {
        super(context, resource, textViewResourceId, objects);
    }

    public void setSelectedItem(int position) {
        selectedItem = position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        if(position == selectedItem) {
            view.setBackgroundResource(R.color.background_material_light);
        } else {
            view.setBackgroundResource(R.color.primary_light);
        }

        return view;
    }
}
