/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;

import im.miet.eventmanagement.R;
import im.miet.eventmanagement.activities.TeamSelectorActivity;
import im.miet.eventmanagement.activities.TeamsDetailedActivity;
import im.miet.eventmanagement.dataclasses.Team;
import im.miet.eventmanagement.fragments.TeamsGridFragment;

/**
 * Created by vlad on 17.08.15.
 */
public class TeamsGridAdapter extends RecyclerView.Adapter {

    private ArrayList<Team> dataSet;
    private int mode;
    private Context mContext;

    public TeamsGridAdapter(Context context, int mode) {
        dataSet = new ArrayList<>();
        this.mode = mode;
        if(mode == TeamsGridFragment.MODE_SELECT) {
            dataSet.add(Team.withoutTeamInstance());
        }
        mContext = context;
    }

    public ArrayList<Team> getData() {
        return dataSet;
    }

    public void setData(ArrayList<Team> newDataSet) {
        if(mode == TeamsGridFragment.MODE_SELECT) {
            dataSet.addAll(newDataSet);
        } else {
            dataSet = newDataSet;
        }

        ((Activity) mContext).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.team_card, parent, false);
        return new ItemHolder(view);

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        ((ItemHolder) holder).title.setText(dataSet.get(position).name);
        if (dataSet.get(position).description.length() > 150) {
            ((ItemHolder) holder).description.setText(dataSet.get(position).description.substring(0,150)+" ...");
        } else {
            ((ItemHolder) holder).description.setText(dataSet.get(position).description);
        }
        ((ItemHolder) holder).card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String teamUrl = dataSet.get(position).url;
                if (mode == TeamsGridFragment.MODE_OPEN_DETAILED) {
                    Intent intent = new Intent(mContext, TeamsDetailedActivity.class);
                    intent.putExtra(TeamsDetailedActivity.URL_TAG, teamUrl);
                    mContext.startActivity(intent);
                } else {
                    Intent intent = new Intent();
                    intent.putExtra(TeamSelectorActivity.RESULT_TAG, teamUrl);
                    TeamSelectorActivity activity = (TeamSelectorActivity) mContext;
                    activity.returnResult(intent);
                }
            }
        });
    }

    public static class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int columnNumber;
        private int topSpace;
        private int rightSpace;
        private int leftSpace;
        private int bottomSpace;

        public SpacesItemDecoration(int top, int right, int bottom, int left, int columns) {
            topSpace = top;
            rightSpace = right;
            leftSpace = left;
            bottomSpace = bottom;
            columnNumber = columns;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.right = rightSpace;
            outRect.bottom = bottomSpace;
            outRect.left = leftSpace;
            if (parent.getChildLayoutPosition(view) < columnNumber)
                outRect.top = topSpace;
        }
    }

    public class ItemHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public TextView description;
        public View card;

        public ItemHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            description = (TextView) itemView.findViewById(R.id.descriptionContainer);
            card = itemView;
        }
    }
}
