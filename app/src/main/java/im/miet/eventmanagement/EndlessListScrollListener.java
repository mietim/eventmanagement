/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import im.miet.eventmanagement.fragments.EndlessScrollingFragment;

/**
 * Created by vlad on 21.08.15.
 */
public class EndlessListScrollListener extends RecyclerView.OnScrollListener {
    private static final int VISIBLE_THRESHOLD = 5;
    private LinearLayoutManager layoutManager;
    private EndlessScrollingFragment fragment;

    public EndlessListScrollListener(LinearLayoutManager layoutManager, EndlessScrollingFragment fragment) {
        this.layoutManager = layoutManager;
        this.fragment = fragment;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        int visibleItemCount = recyclerView.getChildCount();
        int totalItemCount = layoutManager.getItemCount();
        int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();

        if (fragment.canBeScrolled() && (totalItemCount - visibleItemCount)
                <= (firstVisibleItem + VISIBLE_THRESHOLD)) {
            fragment.fetchNextPage();
        }
    }

}
