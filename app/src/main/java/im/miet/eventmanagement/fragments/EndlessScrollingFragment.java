/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement.fragments;

/**
 * Created by vlad on 23.08.15.
 */
public interface EndlessScrollingFragment {
    void fetchNextPage();
    boolean canBeScrolled();
}
