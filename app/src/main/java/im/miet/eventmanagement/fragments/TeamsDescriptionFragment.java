/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import im.miet.eventmanagement.R;
import im.miet.eventmanagement.dataclasses.Team;
import im.miet.eventmanagement.dataclasses.User;
import im.miet.eventmanagement.factories.SimpleItemFactory;

/**
 * Created by vlad on 18.08.15.
 */
public class TeamsDescriptionFragment extends Fragment {
    private String description;
    private User curator;

    public void setData(Team team) {
        description = team.description;
        curator = team.curator;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detailed_container, container, false);
        LinearLayout layout = (LinearLayout)view.findViewById(R.id.itemsContainer);
        if(curator != null) {
            layout.addView(new SimpleItemFactory(getActivity(), layout, curator.lastName + " " + curator.firstName, "Куратор").generate());
        }
        //if(!description.equals("")) {
        if(description != null) {
            layout.addView(new SimpleItemFactory(getActivity(), layout,"Описание", description).generate());
        }
        return view;
    }
}
