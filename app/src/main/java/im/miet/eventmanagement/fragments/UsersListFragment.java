/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement.fragments;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import im.miet.eventmanagement.EndlessListScrollListener;
import im.miet.eventmanagement.R;
import im.miet.eventmanagement.adapters.UsersListAdapter;
import im.miet.eventmanagement.dataclasses.User;
import im.miet.eventmanagement.dataclasses.UsersListPage;

/**
 * Created by vlad on 08.08.15.
 */
public class UsersListFragment extends RequestFragment implements EndlessScrollingFragment {

    private RecyclerView recyclerView;
    private ArrayList<User> dataSet;
    private String nextUri;

    public static UsersListFragment newCustomDataInstance(ArrayList<User> newDataSet) {
        UsersListFragment fragment = new UsersListFragment();
        fragment.dataSet = new ArrayList<>();
        fragment.dataSet.addAll(newDataSet);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.users_list_layout, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.attendees_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(new UsersListAdapter(getActivity()));
        recyclerView.addItemDecoration(new UsersListAdapter.SpacesItemDecoration(dpToPx(8)));
        recyclerView.addOnScrollListener(new EndlessListScrollListener(layoutManager, this));
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (dataSet == null) {
            fetchData(url, this);
        } else {
            ((UsersListAdapter) recyclerView.getAdapter()).setData(dataSet);
            openFirstDetailed(true); //This place fixes bug but it should been tested
            /* Fixed bug : 1. Change to false
                           2. Open any group
                           3. Select any user
                           4. Change user's group in another client
                           5. Refresh
                           Result: user's detailed view is still opened -
                           this can cause exceptions when you change users team/status in it
            */
        }
    }

    @Override
    public void onResponse(Response response) throws IOException {
        if (call.isCanceled()) {
            return;
        }
        String dataString = response.body().string();
        response.body().close();
        ObjectMapper mapper = new ObjectMapper();
        final UsersListPage page = mapper.readValue(dataString, UsersListPage.class);
        nextUri = page.next;
        if (page.previous == null) {
            ((UsersListAdapter) recyclerView.getAdapter()).setData(new ArrayList<>(Arrays.asList(page.users)));
            openFirstDetailed();
        } else {
            ArrayList<User> users = ((UsersListAdapter) recyclerView.getAdapter()).getData();
            final int insertStartPosition = users.size();
            users.addAll(new ArrayList<>(Arrays.asList(page.users)));
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    recyclerView.getAdapter().notifyItemRangeInserted(insertStartPosition, page.users.length);
                }
            });
        }
        isPending = false;
        stopParentActivityRefreshing();
    }

    public void openFirstDetailed() {
        openFirstDetailed(false);
    }

    public void openFirstDetailed(final boolean forceOpen) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (recyclerView.getAdapter().getItemCount() == 0) return;
                if ((getFragmentManager().findFragmentByTag(UsersDetailedFragment.class.getName()) == null) || forceOpen) {
                    UsersDetailedFragment detailedFragment = new UsersDetailedFragment();
                    Bundle args = new Bundle();
                    User firstItem = ((UsersListAdapter) recyclerView.getAdapter()).getData().get(0);
                    args.putString(UsersDetailedFragment.URL_TAG, firstItem.url);
                    detailedFragment.setArguments(args);
                    FragmentTransaction transaction = getFragmentManager()
                            .beginTransaction();
                    transaction.replace(R.id.usersDetailedContainer, detailedFragment,
                            UsersDetailedFragment.class.getName());
                    transaction.commit();
                }
            }
        });
    }

    public void removeFromTeam(int userId) {
        UsersListAdapter adapter = (UsersListAdapter) recyclerView.getAdapter();
        User user = adapter.findById(userId);
        int position = adapter.getData().indexOf(user);
        adapter.getData().remove(user);
        adapter.notifyItemRemoved(position);
    }

    @Override
    public void refresh() {
        fetchData(url, this);
    }

    public void changeStatus(int userId, int status) {
        UsersListAdapter adapter = (UsersListAdapter) recyclerView.getAdapter();
        User user = adapter.findById(userId);
        int position = adapter.getData().indexOf(user);
        user.status.status = status;
        adapter.notifyItemChanged(position);
    }

    @Override
    public void fetchNextPage() {
        fetchData(nextUri, this);
    }

    @Override
    public boolean canBeScrolled() {
        return ((nextUri != null) && (!isPending));
    }
}
