/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.okhttp.Response;

import java.io.IOException;

import im.miet.eventmanagement.HttpHelper;
import im.miet.eventmanagement.R;
import im.miet.eventmanagement.activities.MainActivity;
import im.miet.eventmanagement.activities.TeamSelectorActivity;
import im.miet.eventmanagement.activities.TeamsDetailedActivity;
import im.miet.eventmanagement.adapters.StatusSpinnerAdapter;
import im.miet.eventmanagement.dataclasses.Status;
import im.miet.eventmanagement.dataclasses.User;
import im.miet.eventmanagement.factories.DetailedHeaderFactory;
import im.miet.eventmanagement.factories.DropdownStatusFactory;
import im.miet.eventmanagement.factories.SimpleItemFactory;
import im.miet.eventmanagement.factories.TelephoneItemFactory;

/**
 * Created by vlad on 12.08.15.
 */
public class UsersDetailedFragment extends RequestFragment {


    private User user;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.detailed_container, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        fetchData(url, this);
    }

    @Override
    public void onResponse(Response response) throws IOException {
        if (call.isCanceled()) {
            return;
        }
        String dataString = response.body().string();
        response.body().close();
        ObjectMapper mapper = new ObjectMapper();
        this.user = mapper.readValue(dataString, User.class);
        setUser();
        isPending = false;
        stopParentActivityRefreshing();
    }

    public void changeStatus(int userId, Status status) {
        if (userId == user.id) {
            user.status = status;
            setUser();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            String teamUrl = data.getStringExtra(TeamSelectorActivity.RESULT_TAG);
            String body;
            if (teamUrl == null) {
                body = "{\"team\":null}";
            } else {
                body = "{\"team\":" + "\"" + teamUrl + "\"}";
            }
            HttpHelper.getInstance().callPutRequest(user.teamMembership.url, body);
            if ((user.teamMembership.team != null) && (!user.teamMembership.team.equals(teamUrl)) && (getActivity() instanceof TeamsDetailedActivity)) {
                UsersListFragment listFragment = (UsersListFragment) getActivity()
                        .getFragmentManager()
                        .findFragmentByTag(UsersListFragment.class.getName());
                listFragment.removeFromTeam(user.id);
                getActivity().getFragmentManager()
                        .beginTransaction()
                        .remove(this)
                        .commit();
            } else {
                fetchData(url, this);
            }

        }
    }

    public void setUser() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LinearLayout layout = (LinearLayout) getView().findViewById(R.id.itemsContainer);
                layout.removeAllViews();
                layout.addView(new DetailedHeaderFactory(getActivity(), layout, user.lastName + " "
                        + user.firstName + " "
                        + user.middleName).generate());
                if (!user.email.equals("")) {
                    layout.addView(new SimpleItemFactory(getActivity(), layout, user.email, "Email").generate());
                }

                if (!user.telephone.equals("")) {
                    layout.addView(new TelephoneItemFactory(getActivity(), layout, user.telephone, "Телефон").generate());
                }
                String teamName = (user.teamMembership.team == null) ? "Нет команды" : user.teamMembership.teamName;
                View team = new SimpleItemFactory(getActivity(), layout, teamName, "Команда").generate();
                team.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), TeamSelectorActivity.class);
                        startActivityForResult(intent, 0);
                    }
                });
                layout.addView(team);
                StatusSpinnerAdapter statusSpinnerAdapter = new StatusSpinnerAdapter(getActivity());
                layout.addView(new DropdownStatusFactory(getActivity(), layout, "Статус", statusSpinnerAdapter, user).generate());
            }
        });
    }

    @Override
    public void refresh() {
        fetchData(url, this);
    }
}
