/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.okhttp.Response;
import android.support.v7.widget.RecyclerView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import im.miet.eventmanagement.EndlessGridScrollListener;
import im.miet.eventmanagement.R;
import im.miet.eventmanagement.adapters.TeamsGridAdapter;
import im.miet.eventmanagement.dataclasses.Team;
import im.miet.eventmanagement.dataclasses.TeamsListPage;

/**
 * Created by vlad on 17.08.15.
 */
public class TeamsGridFragment extends RequestFragment implements EndlessScrollingFragment {

    public static final int MODE_SELECT = 0;
    public static final int MODE_OPEN_DETAILED = 1;

    public static final String REACTION_MODE = "im.miet.eventmanagement.fragments.TeamsListFragment.mode";

    private RecyclerView recyclerView;
    private int mode;
    private String nextUri;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null) {
            mode = getArguments().getInt(REACTION_MODE);
        } else  {
            mode = MODE_OPEN_DETAILED;
        }
    }

    @Override
    public void onResponse(Response response) throws IOException {
        if(call.isCanceled()) {
            return;
        }
        String dataString = response.body().string();
        response.body().close();
        ObjectMapper mapper = new ObjectMapper();
        final TeamsListPage page = mapper.readValue(dataString, TeamsListPage.class);
        nextUri = page.next;
        if(page.previous == null) {
            ((TeamsGridAdapter) recyclerView.getAdapter()).setData(new ArrayList<>(Arrays.asList(page.teams)));
        } else {
            ArrayList<Team> teams = ((TeamsGridAdapter) recyclerView.getAdapter()).getData();
            final int insertStartPosition = teams.size();
            teams.addAll(new ArrayList<>(Arrays.asList(page.teams)));
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    recyclerView.getAdapter().notifyItemRangeInserted(insertStartPosition,page.teams.length);
                }
            });
        }
        isPending = false;
        stopParentActivityRefreshing();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.teams_grid_layout, container, false);
        recyclerView = (RecyclerView)view.findViewById(R.id.teams_list);
        recyclerView.setAdapter(new TeamsGridAdapter(getActivity(), mode));
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        //layoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS);
        recyclerView.setLayoutManager(layoutManager);
        //recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        recyclerView.addItemDecoration(new TeamsGridAdapter.SpacesItemDecoration(dpToPx(24), dpToPx(8), dpToPx(24), dpToPx(8), 3));
        recyclerView.addOnScrollListener(new EndlessGridScrollListener((StaggeredGridLayoutManager) recyclerView.getLayoutManager(), this));
        //recyclerView.addOnScrollListener(new EndlessListScrollListener((LinearLayoutManager)recyclerView.getLayoutManager(),this));
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        fetchData(url, this);
    }

    @Override
    public boolean canBeScrolled() {
        return ((nextUri != null)&&(!isPending));
    }

    @Override
    public void fetchNextPage() {
        fetchData(nextUri,this);
    }

    @Override
    public void refresh() {
        fetchData(url,this);
    }
}
