/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import im.miet.eventmanagement.R;
import im.miet.eventmanagement.ServerUrl;
import im.miet.eventmanagement.activities.MainActivity;
import im.miet.eventmanagement.adapters.NavigationAdapter;

/**
 * Created by vlad on 07.08.15.
 */
public class NavigationFragment extends Fragment {


    public static final String FRAGMENT_TAG = "im.miet.eventmanagement.fragments.NavigationFragment";

    //static final int EVENT = 0;
    static final int ATTENDEES = 0;
    static final int TEAMS = 1;
    static final int ORGANIZERS = 2;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.navigation_layout, container, false);
        ListView drawerList = ((ListView) view.findViewById(R.id.drawer_list));
        String[] items = getResources().getStringArray(R.array.Navigation);
        NavigationAdapter adapter = new NavigationAdapter(getActivity(),
                R.layout.navigation_list_item, items);
        drawerList.setAdapter(adapter);
        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ((NavigationAdapter) parent.getAdapter()).setSelectedItem(position);
                ((NavigationAdapter) parent.getAdapter()).notifyDataSetChanged();
                constructLayout(position);
            }
        });
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        constructLayout(ATTENDEES);
    }

    private void constructLayout(int position) {
        switch (position) {
            case ATTENDEES:
                constructUsersLayout(ServerUrl.ATTENDEES_URL);
                break;
            case TEAMS:
                constructTeamsLayout(ServerUrl.TEAMS_URL);
                break;
            case ORGANIZERS:
                constructUsersLayout(ServerUrl.ORGANIZERS_URL);
                break;
            default:
                break;
        }
    }

    private void clearLayout() {
        ((ViewGroup) getActivity().findViewById(R.id.contentContainer)).removeAllViews();
        ArrayList<RequestFragment> fragments = ((MainActivity) getActivity()).getAllRequestFragments();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        for (RequestFragment fragment : fragments) {
            transaction.remove(fragment);
        }
        transaction.commit();
    }

    private void constructUsersLayout(String uri) {
        clearLayout();
        LayoutInflater.from(getActivity()).inflate(R.layout.users_layout,
                (ViewGroup) getActivity().findViewById(R.id.contentContainer), true);
        UsersListFragment attendees = new UsersListFragment();
        Bundle args = new Bundle();
        args.putString(UsersListFragment.URL_TAG, uri);
        attendees.setArguments(args);
        FragmentTransaction transaction = getActivity().getFragmentManager().beginTransaction();
        transaction.replace(R.id.usersListContainer, attendees,
                UsersListFragment.class.getName());
        transaction.commit();
    }

    private void constructTeamsLayout(String uri) {
        clearLayout();
        LayoutInflater.from(getActivity()).inflate(R.layout.teams_layout,
                (ViewGroup) getActivity().findViewById(R.id.contentContainer), true);

        TeamsGridFragment teams = new TeamsGridFragment();
        Bundle args = new Bundle();
        args.putString(TeamsGridFragment.URL_TAG, uri);
        args.putInt(TeamsGridFragment.REACTION_MODE, TeamsGridFragment.MODE_OPEN_DETAILED);
        teams.setArguments(args);
        FragmentTransaction transaction = getActivity().getFragmentManager().beginTransaction();
        transaction.replace(R.id.teamsContainer, teams,
                TeamsGridFragment.class.getName());
        transaction.commit();
    }
}
