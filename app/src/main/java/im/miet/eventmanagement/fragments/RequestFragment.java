/*
 * Copyright (c) 2015 Verba Vladislav. All rights reserved. Use of this source code is governed by GNU GPL-2.0 license that can be found in the LICENSE file.
 */

package im.miet.eventmanagement.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.Toast;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

import im.miet.eventmanagement.HttpHelper;
import im.miet.eventmanagement.R;
import im.miet.eventmanagement.activities.MainActivity;

/**
 * Created by vlad on 07.08.15.
 */
public abstract class RequestFragment extends Fragment implements Callback {

    public static final String URL_TAG = "im.miet.eventmanagement.url";

    protected String url;
    protected Call call;
    protected boolean isPending = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            url = getArguments().getString(URL_TAG);
        }
    }

    public synchronized boolean isPending() {
        return isPending;
    }

    @Override
    public void onStop() {
        super.onStop();
        if(call != null) {
            isPending = false;
            call.cancel();
        }
    }

    @Override
    public void onFailure(Request request, IOException e) {
        if(call.isCanceled()) {
            return;
        }
        isPending = false;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getActivity(), getResources().getString(R.string.err_bad_connection),
                        Toast.LENGTH_LONG).show();
            }
        });
        stopParentActivityRefreshing();
    }

    protected void stopParentActivityRefreshing() {
        if (getActivity() instanceof MainActivity) {
            ((MainActivity)getActivity()).onRefreshingStopped();
        }
    }

    public void fetchData(String url, Callback callback) {
        isPending = true;
        call = HttpHelper.getInstance().callGetRequest(url,callback);
    }

    abstract public void refresh();

    protected int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }
}
